//Operators

/*
	+ Sum
	- Difference
	* Product
	/ Quotient
	% Remainder

	+= Addition
	-= Subtraction
	*= Multiplication
	/= Division
	%= Modulo
*/

function mod(){
	return 9%2;
}

// console.log (mod())


// Assignement operator

	let x = 1;

	let sum = 1;
	// sum = sum + 1;
	sum += 1;

// console.log (sum)


// Increment & Decrement (++/--)
// Operators that add/subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to


let z=1

let increment = ++z;

// Pre-encrement
// console.log("Result of pre-increment"+increment);
// console.log("Result of pre-increment: " + z);


// Post-increment
increment = z++
// console.log ("Result of post-increment: " + increment);
// console.log ("Result of post-increment: " + z);


// Pre-decrement
let decrement = --z
// console.log("Result of pre-decrement: " + decrement);
// console.log("Result of pre-decrement: " + z);

// Post-decrement
decrement = z--
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


// Comparison Operators

// Equality Operator (==)

let juan = 'juan';

console.log(1 == 1);
console.log (0 == false);
console.log ('juan' == juan);

// Strict Equality (===)
console.log(1 === true)

// Inequality Operator (!=)

console.log("Inequality Operator")
console.log(1 != 1);
console.log('Juan' != juan);

// Strict inequality (!==)
console.log (0 !== false);

// Other Comparison Operators
/*
	> Greater than
	< Less than
	>= Greater than or equal
	<= Less than or equal
*/

// Logical Operators

/*
	And Operator (&&) - returns true if all operands are true
		true + true = true
		true + false = false
		false + true = false
		false + false = false
	Or Operator (||) - returns true if atleast one of the operands is true
		true + false 
*/

// And Operator
let isLegalAge = true
let isRegistered = true


allRequirementsMet = isLegalAge && isRegistered;
console.log('Result of logical AND operator: ' + allRequirementsMet);

// Or Operator
allRequirementsMet = isLegalAge || isRegistered;
console.log('Result of logical OR operator: ' + allRequirementsMet);


// Selection Control Structures

/*
	IF statement
		-execute a commande if a specified condition is true
			if (condition) {
				statement/s
			}
*/

let num = -1;

if (num<0) {
	console.log('Hello')
}

let value = 112354342;

if (value>=10) {
	console.log('Welcome to Zuitt')
}

/*
	If-Else Statement
		executes a statement if the previous condition returns false.
			if (condition){
				statements;
			}
			else {
				statement/s;
			}

*/
num = 5
if (num >= 10) {
	console.log('Number is greater or equal to 10')
}
else{
	console.log('Number is not greater or equal to 10')
}

/*
num = 65.5
if (num > 59) {
	console.log('Senior Age')
}
else{
	console.log('Invalid Age')
}

num = parseInt(prompt('Please provide age:'));
if (num > 59) {
	alert('Senior Age')
}
else{
	alert('Invalid Age')
}*/

// If-ElseIf-Else Statement
		
/*
if (condition) {
	statement
}
else if{
	statement
}
elseif {
	statement
}
elseif {
	statement
}
elseif {
	statement
}
.
.
.
else {
	statement
}
*/
/*
	1 Quezon
	2 Valenzuela
	3 Pasig
	4 Taguig
*/

/*let city = parseInt (prompt("Enter a number"));
if (city === 1){
	alert('Welcome to Quezon City')
}
else if (city === 2){
	alert('Welcome to Valenzuela City')
}
else if (city === 3){
	alert('Welcome to Pasig City')
}
else if (city === 4){
	alert('Welcome to Taguig City')
}
else{
	alert('Invalid Number')
}*/


let message = '';

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30) {
		return 'Not a typhoon, yet';
	}
	else if (windSpeed <=61) {
		return 'Tropical Depression detected'
	}
	else if (windSpeed >=62 && windSpeed <=88) {
		return 'Tropical Storm detected'
	}
	else if (windSpeed >=89 && windSpeed <=117) {
		return 'Severe Tropical Storm detected'
	}
	else {
		return 'Typhoon detected'
	}
}


message = determineTyphoonIntensity(70)
console.log(message);


/*Ternary Operator
	(condition) ? ifTrue : ifFalse
*/


let ternaryResult = (1 < 18) ? true : false;
console.log('Result of Ternary Operator: ' + ternaryResult);

let name;
function isOfLegalAge(){
	name = 'John';
	return 'You are of the age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

/*let age = parseInt(prompt('What is your age?'))
let LegalAge = (age>=18)? isOfLegalAge() : isUnderAge();
alert('Result of Ternary Operator in Function: ' + LegalAge + ', ' + name);*/

/*
	Switch Statement
		switch (expression) {
			case value1:
				statement/s;
				break;
			case value2:
				statement/s;
			case valueN:
				statement/s;
			default:
				statement/s;
		}
*/
/*
let day = prompt('What day of the week is it today?') .toLowerCase();

switch (day){
	case 'sunday':
		alert('The color of the day is red');
		break;
	case 'monday':
		alert('The color of the day is orange');
		break;
	case 'tuesday':
		alert('The color of the day is yellow');
		break;
	case 'wednesday':
		alert('The color of the day is green');
		break;
	case 'thursday':
		alert('The color of the day is blue');
		break;
	case 'friday':
		alert('The color of the day is indigo');
		break;
	case 'saturday':
		alert('The color of the day is violet');
		break;
	default:
		alert('Please input a valid day');
}*/

/*
	Try-Catch-Finally Statement
		commonly used for error handling
*/

function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log (typeof error);
		console.warn (error.message);
	}
	finally {
		alert('Intensity updates will show new alert')
	}
}

showIntensityAlert(56);